from urllib2 import urlopen
import json
import subprocess, shlex

allProjects     = urlopen("https://gitlab.com/api/v3/groups/t3chfest?private_token=KrYUpwx8bDctf14x99xR")

allProjectsDict = json.loads(allProjects.read().decode())

for thisProject in allProjectsDict['projects']: 
    try:
        
        thisProjectURL  = thisProject['http_url_to_repo']
        command     = shlex.split('git clone %s' % thisProjectURL)
        resultCode  = subprocess.Popen(command)

    except Exception as e:
        print("Error on %s: %s" % (thisProjectURL, e.strerror))